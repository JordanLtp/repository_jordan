package com.example.test.cookyapp;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.w3c.dom.Text;



public class NomRecette extends AppCompatActivity {


    private ListView mListView;
    private ListView mListView2;

    private ArrayAdapter<String> arrayAdapterList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nom_recette);

        String[] recette = {"battre les oeufs et la farine", "Prechauffer le four 200°", "Diminuer le feu", "Mettre les pates", "Egoutez les pates", "Degustez !"};
        String[] ingredients = {"Beure", "farine", "huille", "Oignon", "Ail ", "Chocolat"};


        mListView = (ListView) findViewById(R.id.lvRecette);
        arrayAdapterList = new ArrayAdapter<String>(NomRecette.this, android.R.layout.simple_list_item_1, recette);
        mListView.setAdapter(arrayAdapterList);


        mListView2 = (ListView) findViewById(R.id.lvIngredients);
        arrayAdapterList = new ArrayAdapter<String>(NomRecette.this, android.R.layout.simple_list_item_1, ingredients);
        mListView2.setAdapter(arrayAdapterList);

    }


}








